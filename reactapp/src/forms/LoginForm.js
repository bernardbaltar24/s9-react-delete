import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';

const LoginForm = (props) => {
	const[formData, setFormData] = useState({
    email: "",
    password: ""
  })

  const [disabledBtn, setDisabledBtn] = useState(true)

  const [isRedirected, setIsRedirected] = useState(false)

  const {email, password} = formData;

  const onChangeHandler = e => {
    // console.log(e);
    // console.log(e.target);

    //Backticks - multi-line and string interpolation
    // console.log(`e.target.name : ${e.target.name} `)
    // console.log(`e.target.value : ${e.target.value} `)

    //update the state
    setFormData({
      ...formData,
      [e.target.name] : e.target.value
    })
  }

  const onSubmitHandler = async e => {
    e.preventDefault();

    const memberLogin = {
      email,
      password
    }

    try{
      const config = {
        headers: {
          'Content-Type' : 'application/json'
        }
      }
      const body = JSON.stringify(memberLogin)
      const res = await axios.post("http://localhost:5000/members/login", body, config)
      localStorage.setItem("token", res.data.token)
      localStorage.setItem("username", res.data.member.username)
      setIsRedirected(true)
      console.log(res)
    }catch(e){
      localStorage.removeItem("token")
      console.log(e)

    }

  }

  //USE EFFECT
  //Hook that tells the component what to do after render
  useEffect(()=>{
  if(email !== "" && password !== ""){
    setDisabledBtn(false) 
  }else{
    setDisabledBtn(true)
  }
  }, [formData])

if(isRedirected){
  // return <Redirect to="/profile" />
  window.location="/profile"
}
  return (
    <Form onSubmit={ e => onSubmitHandler(e)}>
      <FormGroup>
        <Label for="email" className="mt-3">email/Email</Label>
        <Input 
        type="email" 
        name="email" 
        id="email" 
        placeholder="Please input email"
        value={email}
        onChange={e => onChangeHandler(e)}
        // onChange={e => setUsername(e.target.value)}
        maxLength="30"
        // pattern="[a-zA-Z0-9]+"
        required
         />
      </FormGroup>

      <FormText>Please enter valid Username or Email</FormText>

      <FormGroup>
        <Label for="password">Password</Label>
        <Input 
        type="password" 
        name="password" 
        id="password" 
        placeholder="Please input Password"
        value={password}
        onChange={e => onChangeHandler(e)}
        // onChange={e => setPassword(e.target.value)}
        required
        minLength="5"
        />
      </FormGroup>

     <Button 
      className="mb-3 btn btn-block"
      disabled={disabledBtn}
      >Login</Button>
      <p>
      You don't have an account? Please <Link to="/register">Register</Link>.
      </p>
    </Form>
  );
}

export default LoginForm;