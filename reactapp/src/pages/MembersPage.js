import React, {useState, useEffect} from 'react';
import { Container, Row, Col } from 'reactstrap';
import {Redirect, BrowserRouter, Route, Switch } from 'react-router-dom';
import MemberForm from '../forms/MemberForm';
import MemberTableHead from '../tables/MemberTableHead';
import axios from 'axios';


const MembersPage = (props) => {

  console.log("MEMBERSPAGE TOKEN", props.token)

    const[membersData, setMembersData] = useState({
      token: props.token,
      members: []
    })

    const {token, members} = membersData;

    console.log("MEMBERSPAGE22 TOKEN", token)

    const getMembers = async () => {
      try{
        const config = {
        headers : {
          Authorization: `Bearer ${token}`
        }
      }

        const res = await axios.get("http://localhost:5000/members", config)
        console.log("MEMBERS PAGE res", res)

        return setMembersData({
          ...membersData,
          members: res.data
        })
      }catch(e){
        console.log("MEMBERS ERROR", e)
      }
    }

//------------------DELETEMEMBER--------------------

    const deleteMember = async(id) => {
      try{
        const config = {
        headers : {
          Authorization: `Bearer ${token}`
          }
        }

        const res = await axios.delete(`http://localhost:5000/members/${id}`, config)

        getMembers()

      } catch(e){
        console.log("ERROR SA DELETE MEMBER", e)
      }
    }
  
  useEffect(() => {
    getMembers()
  },[setMembersData])
  

  return (
    <Container>
      <Row className="mb-5">
        <Col>
        	<h1>MEMBERS Page</h1>
        </Col>
      </Row>
      <Row className="d-flex">
        <Col md="4" className="border">
        	<MemberForm/>
        </Col>
        <Col md="8" className="">
        	<MemberTableHead membersAttr={members} deleteMember={deleteMember} />
        </Col>
      </Row>
    </Container>
  );
}

export default MembersPage;