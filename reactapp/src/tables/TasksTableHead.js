import React from 'react';
import { Table } from 'reactstrap';
import TasksTableBody from '../rows/TasksTableBody';

const TaskTables = (props) => {
  let row;

    if(!props.tasksAttr){ //pwedeng ipasok ang if sa loob ng return by using ternary = !props.teamAttr?statement:result
      row = (
        <tr colSpan="3">
          <em>No Tasks found..</em>
        </tr>
        )
    }else{
      let i = 0;
      row=(
        props.tasksAttr.map( tasks => {return <TasksTableBody tasksAttr={tasks} key={tasks._id} index={++i} />
        })
      )
    }

  return (
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Description</th>
          <th>Full Name</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {row}
      </tbody>
    </Table>
  );
}

export default TaskTables;