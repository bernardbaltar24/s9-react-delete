import React, { Fragment } from 'react';
import {Link} from 'react-router-dom';
import { Table } from 'reactstrap';

const MembersTableRow = ({membersAttr, index, deleteMember}) => {
  console.log("MEMBER ROW ATTR", membersAttr)

  const {username, email, _id, teamId, position, isActive}=membersAttr;
  return (
    <Fragment>
        <tr>
          <th scope="row">{index}</th>
          <td>{username}</td>
          <td>{email}</td>
          <td>{teamId ? teamId.name: "N/A"}</td>
          <td>{position}</td>
          <td>{isActive ? "Active" : "Deactivated"}</td>
          <td>
            <Link className="btn btn-primary" to={`/members/${_id}`}><i class="far fa-eye"></i></Link>
            <button className="btn btn-warning ml-1"><i class="far fa-edit"></i></button>
            <button className="btn btn-danger ml-1" onClick={()=>deleteMember(_id)} ><i class="far fa-trash-alt"></i></button>
          </td>
        </tr>
    </Fragment>
  );
}

export default MembersTableRow;