import React, { Fragment } from 'react';
import { Table } from 'reactstrap';

// const TeamTable = (props) => {

//   const team = props.teamsAttr

//   return (
//     <Fragment>
//         <tr>
//           <th scope="row">{props.index}</th>
//           <td>{team.name}</td>
//           <td>
//             <button className="btn btn-primary ml-1"><i class="far fa-eye"></i></button>
//             <button className="btn btn-warning ml-1"><i class="far fa-edit"></i></button>
//             <button className="btn btn-danger ml-1"><i class="far fa-trash-alt"></i></button>
//           </td>
//         </tr>
//     </Fragment>
//   );
// }

//or you can use 

// export default TeamTable;

const TeamTable = ({teamsAttr, index, deleteTeam}) => {

  return (
    <Fragment>
        <tr>
          <th scope="row">{index}</th>
          <td>{teamsAttr.name}</td>
          <td>{teamsAttr.isActive? "Active" : "Deactivated"}</td>
          <td>
            <button className="btn btn-primary ml-1"><i class="far fa-eye"></i></button>
            <button className="btn btn-warning ml-1"><i class="far fa-edit"></i></button>
            <button className="btn btn-danger ml-1"><i class="far fa-trash-alt" onClick={()=>deleteTeam(teamsAttr._id)}></i></button>
          </td>
        </tr>
    </Fragment>
  );
}

export default TeamTable;
